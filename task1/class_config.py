#!/usr/bin/env python3

# Piotr Wieczorek (pwieczo@gmail.com)
# Helper class - stores global config in CONFIG object
# v1.0


class CONFIG:
     
      ### dict for variables
      configuration = {}

      ### number of arguemnts for the script
      ARGS = 2

      ### Redis connecton object
      DBCONN = None
