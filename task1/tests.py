#!/usr/bin/env python3

# Piotr Wieczorek (pwieczo@gmail.com)
# Set of tests =>  DBLOADER 
# v1.0  [17.07.2019]


import sys,redis,os.path
from class_config import CONFIG



### test0 -> script arguments
def test_args(argv):
    return len(argv) == int(CONFIG.ARGS)


### test1 -> csv file
def test_csv_file(csvfile):
    return os.path.exists(csvfile)


### test2 -> config
def test_config_file(conffile):
    return os.path.exists(conffile)


### test3 -> redis connection
def test_redis_connection():
    try: 
        ### connection params 
        try:
            r = redis.Redis(host=CONFIG.configuration['dbhost'], port=int(CONFIG.configuration['dbport']), db=int(CONFIG.configuration['dbid']),
                socket_timeout=int(CONFIG.configuration['dbtimeout']), socket_connect_timeout=int(CONFIG.configuration['dbtimeout']),
                decode_responses=True
                )
        except Exception as err:
               print("Error in conenction string: %s %s"%(err,repr(err)))
               return False
        r.ping()
        CONFIG.DBCONN = r

        return  True

    except (redis.exceptions.ConnectionError, redis.exceptions.BusyLoadingError):
        return False

