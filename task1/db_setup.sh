#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Illegal number of arguments! Exiting ..."
    echo ""
    echo "USAGE $0 REDIS_PORT_NUMBER"
    echo ""
    exit 1
fi

REDISPORT=$1
SCANNED=0
USEDBY=""

## assuming docker in installed / commands depends on OS
MYDOCKER=`which docker|grep "no docker"|wc -l `
if [ $MYDOCKER -gt "0" ]; then
   echo "Docker not in path or docker not installed. Please check Your configuration. Exiting ..."
   exit 2
fi



IFS=$'\n'
for CONTAINER in `docker ps| grep -v "CONTAINER ID"|awk {'print $1'}`
do
    PORTS=`docker port $CONTAINER| grep $REDISPORT|wc -l`
    if [ $PORTS -gt "0" ]; then
       USEDBY=$USEDBY" "$CONTAINER" "
    fi   
    SCANNED=$(( $SCANNED + $PORTS ))
done    


if [ $SCANNED -eq "0" ]; then
   docker rm dbredis	
   docker run --name dbredis -p $REDISPORT:$REDISPORT  -d redis
else
   echo "ERROR:"	
   echo ""
   echo "PORT $REDISPOORT already taken by$USEDBY!!!"
   echo "Check Your config.ini file"
   echo ""
   exit 3
fi
