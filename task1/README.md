# Task1 - dbloader
Loading csv file from disk (can be replaced with direct url get) into Redis DB

APP requires csv file and config file with DB credentials, which is parsed and validate

## Prerequisite
- docker up and running (installation depends on OS)
- python3 python3-devel gcc  and pip installed and configured (installation depends on OS)
e.g. yum install -y python36 python36-devel python36-pip gcc 


Deploy RedisDB :
- redis server installation in OS e.g. yum install redis.(arch)
- docker: docker run --name dbredis -p 6379:6379  -d redis
- automatic with docker: [./db_setup.sh DESIRED_REDIS_PORT] , script will check if port is not in use


Install python pkgs
```
   pip install -r requirements.txt 
```

Configure config.ini according to yor need, e.g.:
```
  [DBLOADER]
  dbhost=localhost
  dbport=6379
  dbtimeout=2
  dbid=0
```


## Lunch

[ ./db_data_loader.py path_csv_file path_config_file ]

if everything is OK, should see similar summary:

[FINISHED] ./db_data_loader.py executed in 0.2686 [s] ROWS: 887 INSERTED: 887
   

