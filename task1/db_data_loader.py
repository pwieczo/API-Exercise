#!/usr/bin/env python3

# Piotr Wieczorek (pwieczo@gmail.com)
# APP loades provided dataset (CSV) into  DB (Redis). DB credentials are stored in config *.ini file
# v1.0  [17.07.2019]


import sys,csv,ujson,redis,uuid,time,configparser,io
from tests import *


### creates json document 
def make_json(headers=[], values=[]):
    if not len(headers)  == len(values):
       return "ERR"
    document = {}   
    for x in  range (len(headers)):
        document[headers[x]] = values[x] 
    return ujson.dumps(document)    



### parse csv file
def parse_save_csv(csvfile):  
    
    counter_inserted = 0
    counter_rows     = 0

    with open(csvfile, 'r') as fcsv:
         reader = csv.reader(fcsv)
         header = next(reader,None) 

         result = None
         for row in reader:
          counter_rows+=1   
          try:   
             result =  make_json(header,row) 
             if not "ERR" in result:
                CONFIG.DBCONN.set( str(uuid.uuid4()) , result )
                counter_inserted+=1        
             else:
                print("ROW: %s skipped"%str(row)) 
          except Exception as err:
                 print("ERROR processing row: %s err: %s %s"%(str(row),err,repr(err)))

    return counter_inserted,counter_rows                 


### parse and store config
def parse_save_config(configfile):

    try:
        configuration = configparser.ConfigParser(allow_no_value=False)
        configuration.read(configfile)
    except Exception as err:
        print("Error reading config file: %s %s. Exiting ... "%(err,repr(err)))
        sys.exit(5)        

    try:
        for arg in configuration.options("DBLOADER"):
            CONFIG.configuration[arg] = configuration.get("DBLOADER",arg)
    except Exception as err:
        print("Error parsing configuration from conffile : %s %s. Exiting ... "%(err,repr(err)))
        sys.exit(6)  



### main program invocation
def main(argv):

    starttime = float(time.time())

    if not test_args(argv):
       print("\nUSAGE:   %s csvfile_path configfile_path\n"%sys.argv[0])
       sys.exit(1)
    if not test_config_file(sys.argv[2]):
       print("\nConfigFile not Found !!! Exiting ... \n")
       sys.exit(2)

    parse_save_config(sys.argv[2])

    if not test_redis_connection():
       print("\nConfigFile not Found !!! Exiting ... \n")
       sys.argv(4)

    if not test_csv_file(sys.argv[1]):
       print("\nCSV File not Found !!! Exiting ... \n")
       sys.exit(3)

    results = parse_save_csv(sys.argv[1])
    CONFIG.DBCONN = None

    print("\n[FINISHED] %s executed in %.4f [s] ROWS: %s INSERTED: %s\n"%(sys.argv[0],float(time.time())-starttime,results[1],results[0]))

if __name__ == "__main__":
   main(sys.argv[1:])
