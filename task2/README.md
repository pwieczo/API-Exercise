# Task1 - API

API is written in python + flask. It connects with Redis DB and operate on data loaded in task0 (get, modify, add, delete)

## Prerequisite
- python3 and pip installed and configured
- Redis DB from task1 up & running


Install python pkgs
```
   pip install -r requirements.txt 
```

Configure ENVironmentals:
```
    export API_PORT=9095
    export REDIS_PORT=6379	
    export REDIS_HOST=localhost
```


## Lunch

Backend Entrypoint :	[ backend/app.py ]

## Endpoints
API is communicating through the endpoints

### [ /api/v1/people]
Allowed method: GET
Gets list of elements in details (with keys -> uuids)  from DB 

### [ /api/v1/people/<uuid> ]
Allowed method: 
   - GET:	Gets details about specific uuid element
   - DELETE:	Deletes provided uuid from DB
   - PUT:	Modifies data about specified uuid in DB (make sure you use proper header with content type in request)
   - POST:      Adds new data to DB(make sure you use proper header with content type in request)

if any of method is invoked on non-existing element, 404 (not found) will be returned
 

### [ /probe/liveness  ]
Liveness probe - used for checking if API is alive

### [ /probe/readiness ]
Readiness probe which checks if it is possible to open DB connection (for user interaction endpoints)
