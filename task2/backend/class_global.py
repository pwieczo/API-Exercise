#!/usr/bin/env python3

# Piotr Wieczorek (pwieczo@gmail.com)
# v1.0


class GLOBALS:
     
      ### Redis connection
      DBCONN = None

      ### API port
      API_PORT = None

      ### REDIS port
      REDIS_PORT = None

      ### REDIS host
      REDIS_HOST = None
