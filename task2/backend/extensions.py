
import redis,ujson,os
from class_global import GLOBALS
from generator import getuuid



def test_env():

    GLOBALS.API_PORT = os.environ.get('API_PORT') 
    if GLOBALS.API_PORT == None:
       print ("API_PORT not set")
       return False
    GLOBALS.REDIS_PORT = os.environ.get('REDIS_PORT') 
    if GLOBALS.REDIS_PORT == None:
       print ("REDIS_PORT not set")
       return False
    GLOBALS.REDIS_HOST = os.environ.get('REDIS_HOST') 
    if GLOBALS.REDIS_HOST == None:
       print ("REDIS_HOST not set")
       return False
    
    return True 


def connect_db():
    GLOBALS.DBCONN = redis.StrictRedis(host=GLOBALS.REDIS_HOST, port=int(GLOBALS.REDIS_PORT), db=0) 


def test_db():
    try:
       GLOBALS.DBCONN.ping()        
       return True
    except:        
       return False 


def normalize_add_result(json_data,uuid):

    json_data["uuid"] = uuid
    return normalize_result(json_data)


def normalize_result(json_data):

    ### this could a part data normalization during DBload.

    try:    json_data['passengerClass'] =  int(json_data['Pclass'])
    except: pass
    try:    json_data['siblingsOrSpousesAboard'] =  int(json_data['Siblings/Spouses Aboard'])
    except: pass
    try:    json_data['parentsOrChildrenAboard'] =  int(json_data['Parents/Children Aboard'])
    except: pass
    try:    json_data['Age'] = int(json_data['Age'])
    except: pass
    try:    json_data['Fare'] = float(json_data['Fare'])
    except: pass



    try: del  json_data['Pclass']
    except: pass
    try: del  json_data['Siblings/Spouses Aboard']
    except: pass
    try: del  json_data['Parents/Children Aboard']
    except: pass

    try:
        if json_data['Survived'] == "1" : json_data['Survived']  = True
        if json_data['Survived'] == "0" : json_data['Survived']  = False
    except: pass

    return json_data


def get_people():
    db_people = []
    for key in GLOBALS.DBCONN.scan_iter("*"):
        rowvalue =  ujson.loads(GLOBALS.DBCONN.get(key))
        rowvalue['uuid'] = key
        db_people.append(  normalize_result(rowvalue) )
    return db_people    


def get_uuid(uuid):
    if not  GLOBALS.DBCONN.exists(uuid):
       return { }
    return  normalize_add_result( ujson.loads(GLOBALS.DBCONN.get(uuid)), uuid )



def delete_uuid(uuid):
    if not  GLOBALS.DBCONN.exists(uuid):
       return { 'RESULT' : 'FAILED' }
    GLOBALS.DBCONN.delete(uuid) 
    if not  GLOBALS.DBCONN.exists(uuid):
        return { 'RESULT' : 'SUCCESS' } 
    else:
        return { 'RESULT' : 'FAILED' }    


def put_uuid(json_data,uuid):
    if not  GLOBALS.DBCONN.exists(uuid) or json_data == None:
       return { } 
    GLOBALS.DBCONN.set(uuid,ujson.dumps(json_data))
    return normalize_add_result( ujson.loads(GLOBALS.DBCONN.get(uuid)), uuid )
    

def post_person(json_data):
    if json_data == None:
       return { }
    uuid = getuuid()
    GLOBALS.DBCONN.set(uuid,ujson.dumps(json_data))
    return normalize_add_result( ujson.loads(GLOBALS.DBCONN.get(uuid)), uuid )

