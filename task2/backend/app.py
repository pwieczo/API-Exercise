#!/usr/bin/env python3

# Piotr Wieczorek (pwieczo@gmail.com)
# API backend handles clinet's requests

import os, redis, ujson
from extensions import *
from flask import request, Flask, Response

app = Flask(__name__)


@app.before_request
def before_request():
    if not test_db():
       connect_db()


@app.route('/api/v1/people', methods=['GET'])
def api_v1_people():
    try:
        RESULT =  get_people()
        return Response( ujson.dumps(RESULT), 200 , mimetype='application/json' )
    except Exception as err:
        return Response( ujson.dumps( {'ERROR':'Server err: %s'%err} ), 500 , mimetype='application/json' )


@app.route('/api/v1/people/<uuid>', methods=['GET'])
def api_v1_uuid(uuid):
    try:
        RESULT =  get_uuid(uuid)
        if len(RESULT.keys()) > 1: 
           return Response( ujson.dumps(RESULT), 200 , mimetype='application/json' )
        else:
            return Response( ujson.dumps( {}), 404, mimetype='application/json' )
    except Exception as err:
        return Response( ujson.dumps( {'ERROR':'Server err: %s'%err} ), 500 , mimetype='application/json' )


@app.route('/api/v1/people/<uuid>', methods=['DELETE'])
def api_v1_delete(uuid):
    try:
        RESULT =  delete_uuid(uuid)
        if RESULT['RESULT'] == "SUCCESS" : 
            return Response( ujson.dumps({}), 200 , mimetype='application/json' )
        else:  
            return Response(ujson.dumps( {}), 404 , mimetype='application/json' )
    except Exception as err:
        return Response( ujson.dumps( {'ERROR':'Server err: %s'%err} ), 500 , mimetype='application/json' )


@app.route('/api/v1/people/<uuid>', methods=['PUT'])
def api_v1_put(uuid):
    try:
        RESULT =  put_uuid(request.get_json(), uuid)
        if len(RESULT.keys()) > 0:
           return Response( ujson.dumps(RESULT), 200 , mimetype='application/json' )
        else:
           return Response( ujson.dumps( {}), 404 , mimetype='application/json' )
    except Exception as err:
        return Response( ujson.dumps( {'ERROR':'Server err: %s'%err} ), 500 , mimetype='application/json' )


@app.route('/api/v1/people', methods=['POST'])
def api_v1_post():
    try:
       RESULT =  post_person(request.get_json())
       return Response( ujson.dumps(RESULT), 200 , mimetype='application/json' )
    except Exception as err:
       return Response( ujson.dumps( {'ERROR':'Server err: %s'%err} ), 500 , mimetype='application/json' )



@app.route('/probe/liveness')
def liveness_probe():
    return 'ALIVE!'

@app.route('/probe/readiness')
def readiness_probe():
    ### todo : add check DB
    return 'READY'






if __name__ == '__main__':
   try:   
       if test_env(): 
          app.run(port=int(GLOBALS.API_PORT), host='0.0.0.0')
   except Exception as err:
       print ("Server unable to start due to errors: %s %s"%(err,repr(err)))

