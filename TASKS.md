# TASKS 

Hi.

I put my solution for  exercise 1,2,3 and 4  in separate directories. Exercise 3 has only deployment script. Dockerfile for API was added as extra file in task2 directory.

## Task1

Purpose:	setup and load csv data to DB

language:	python

DB:		Redis


## Task2

Purpose:	setup environment and lunch API

language:	python(Flask)

DB:		Redis


## Task3

Purpose:	create portable docker containers

builds:		dockerfiles

imgs:		redis-db 
		api-backend


## Task4

Purpose:	deploy and build docker images as PODs(deployments) and services. Connect containers in order to have full service (API+DB).

platform:	standalone k8s cluster

extension:	some LB/HA frontend for external access to services
