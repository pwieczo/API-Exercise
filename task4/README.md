# K8s cluster

In my sceenrio I have 3 node cluster (2node+master) deployed on VMs (ansible+ovirt+cobbler). 


## kubectl version
```
Client Version: version.Info{Major:"1", Minor:"15", GitVersion:"v1.15.0", GitCommit:"e8462b5b5dc2584fdcd18e6bcfe9f1e4d970a529", GitTreeState:"clean", BuildDate:"2019-06-19T16:40:16Z", GoVersion:"go1.12.5", Compiler:"gc", Platform:"linux/amd64"}

Server Version: version.Info{Major:"1", Minor:"15", GitVersion:"v1.15.0", GitCommit:"e8462b5b5dc2584fdcd18e6bcfe9f1e4d970a529", GitTreeState:"clean", BuildDate:"2019-06-19T16:32:14Z", GoVersion:"go1.12.5", Compiler:"gc", Platform:"linux/amd64"}
```

## kubectl get nodes
```
NAME                   STATUS   ROLES    AGE   VERSION
k8s.mydomain        Ready    master   20d   v1.15.0
k8snode1.mydomain   Ready    <none>   20d   v1.15.0
k8snode2.mydomain   Ready    <none>   20d   v1.15.0
```

On nodes include private docker register

## for node in {1..2}; do echo k8snode${node}. mydomain ; ssh k8snode${node}. mydomain cat /etc/docker/daemon.json| grep regist; done
```
k8snode1.mydomain
 "insecure-registries" : ["cicd.mydomain:5000"]
k8snode2.mydomain
"insecure-registries" : ["cicd.mydomain:5000"]
```

## Deploy deployments and services
- [ kubectl apply -f yamls/db/ ] 			redis deployment
- [ kubectl apply -f yamls/app/ ] 			api deployment

## kubectl get deployment  apiapp redis
```
NAME     READY   UP-TO-DATE   AVAILABLE   AGE
apiapp   1/1     1            1           45m
redis    1/1     1            1           105m
```

## kubectl get svc apiapp redis
```
NAME     TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
apiapp   NodePort       10.111.179.225   <none>        9095:32233/TCP   94m
redis    LoadBalancer   10.111.204.35    <pending>     6379:30863/TCP   104m
```


## curl -s  k8snode1.mydomain:32233/api/v1/people/c0a3f3c1-f9ca-4e61-84d1-f3ffdcabf267 | ../task2/pyjson.py
```
{
  "Age": 36,
  "Fare": 13.0,
  "Name": "Miss. Kate Buss",
  "Sex": "female",
  "Survived": true,
  "parentsOrChildrenAboard": 0,
  "passengerClass": 2,
  "siblingsOrSpousesAboard": 0,
  "uuid": "c0a3f3c1-f9ca-4e61-84d1-f3ffdcabf267"
}
```

## curl -s  k8snode2.mydomain:32233/api/v1/people/c0a3f3c1-f9ca-4e61-84d1-f3ffdcabf267 | ../task2/pyjson.py
```
{
  "Age": 36,
  "Fare": 13.0,
  "Name": "Miss. Kate Buss",
  "Sex": "female",
  "Survived": true,
  "parentsOrChildrenAboard": 0,
  "passengerClass": 2,
  "siblingsOrSpousesAboard": 0,
  "uuid": "c0a3f3c1-f9ca-4e61-84d1-f3ffdcabf267"
}
```

Variables for API are passed through ENVs and Redis IP is taken from DNS "redis.default.svc.cluster.local"

By default we use NodePort, because no external LB provided is added. To achieve HA and LB functions we add ha-proxy frontend :
```
…
listen port_80
  bind HA_IP:80
  mode tcp
  …
  default_backend kubernetes_nodes
…
backend kubernetes_nodes
  balance leastconn
  server k8s1 k8snode1.mydomain : check inter 2000 rise 2 fall 3
  server k8s2 k8snode2.mydomain : check inter 2000 rise 2 fall 3
```

For HA_IP we can configure PCS cluster with virtual IP (can be spread out across all node in k8s cluster ) to which ha-proxy (lb function) will be added. 


[ ./ deployer.sh ] 	- deploy kubernetes deployments and services

[ ./ install.sh ]	- installation of PCS cluster and haproxy service


## pcs status
```
Cluster name: frontend

Stack: corosync

3 nodes configured
2 resources configured

Online: [ ip1 ip2 ip3 ]

Full list of resources:

 Resource Group: HAproxyGroup
     virtual_ip (ocf::heartbeat:IPaddr2):       Started ip1
     haproxy    (systemd:haproxy):      Started ip1

Daemon Status:
  corosync: active/enabled
  pacemaker: active/enabled
  pcsd: active/enabled
```


```
curl -s  vip/api/v1/people/c0a3f3c1-f9ca-4e61-84d1-f3ffdcabf267 | ../task2/pyjson.py
{
  "Age": 36,
  "Fare": 13.0,
  "Name": "Miss. Kate Buss",
  "Sex": "female",
  "Survived": true,
  "parentsOrChildrenAboard": 0,
  "passengerClass": 2,
  "siblingsOrSpousesAboard": 0,
  "uuid": "c0a3f3c1-f9ca-4e61-84d1-f3ffdcabf267"
}
```


# Remarks

In terms of kubernetes i am still learning. As far as I know, much better approach is to deploy ingress/ingress controller (envoy,traefik,nginx etc). This can translate paths to configured service name and port and exposes service outside the k8s cluster.

In docs - domain names for serverrs has been replaced with "mydomain". Any inconsistencies can be a result of this.
