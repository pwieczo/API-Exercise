#!/bin/bash



if [ "$#" -ne 4 ]; then
    echo "Illegal number of arguments! Exiting ..."
    echo ""
    echo "USAGE $0 IP1 IP2 IP3 VIP"
    echo ""
    exit 1
fi


##Prereq
# Provide 3 IP adresses of hosts and 1 VIP
IP1=$1
IP2=$2
IP3=$3
VIP=$4			###  <- Please add this VIP to haproxy,cfg in place of VIP IP address 'bind vip:80'

# download module for haproxy (for pcs daemon - under systemd)
curl -O https://raw.githubusercontent.com/thisismitch/cluster-agents/master/haproxy

# install necessary packages, modules and configs
for host in $IP1 $IP2 $IP3 
do 
   ssh $host yum install -y pcs pacemaker fence-agents-all
   ssh $host firewall-cmd --permanent --add-service=high-availability     	# predefined service
   ssh $host firewall-cmd --add-service=high-availability			# predefined service
   ssh $host systemctl enable pcsd.service haproxy.service
   ssh $host systemctl start pcsd.service haproxy.service
   ssh $host "echo 'pass' | passwd 'hacluster'  --stdin"
   scp $host haproxy.cfg /etc/haproxy/
   scp haproxy $host:/usr/lib/ocf/resource.d/heartbeat/
done
rm -f haproxy


# authorize cluster
pcs cluster auth $IP1 $IP2 $IP3
Username: hacluster								# provide user manually
Password:									# provide user manually
#10.14.153.156: Authorized
#10.14.153.157: Authorized
#10.14.153.155: Authorized

# start and enable cluster
pcs cluster setup --start --name frontend $IP1 $IP2 $IP3
pcs cluster enable --all
#10.14.153.155: Cluster Enabled
#10.14.153.156: Cluster Enabled
#10.14.153.157: Cluster Enabled

#switchoff fencing
pcs property set stonith-enabled=false										# no fence device

#create first resource - vip
pcs resource create virtual_ip ocf:heartbeat:IPaddr2 ip=$VIP cidr_netmask=32 op monitor interval=30s
# create second resource - haproxy service
pcs resource create haproxy systemd:haproxy  op monitor interval=10s

# create group - all resources on one node
pcs resource group add HAproxyGroup virtual_ip haproxy
# set order - first IP is up , after haproxy service
pcs constraint order set virtual_ip haproxy

