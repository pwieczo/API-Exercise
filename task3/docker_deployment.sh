#!/bin/bash


# Redis
docker run -d --name dbredis --network=host redis

# API
docker build -t api ../task2/backend
docker run -d --name api --network=host -e REDIS_HOST=localhost -e REDIS_PORT=6379 -e API_PORT=9096 --restart=always api

# docker Registry
# prepare images to be downloaded by nodes in k8s cluster
docker run -d -p 5000:5000 --restart always --name registry registry:2.4.0
if [ $? -eq "0" ]; then
   docker tag api localhost:5000/api
   docker push localhost:5000/api
   docker tag redis localhost:5000/redis
   docker push localhost:5000/redis
fi   
